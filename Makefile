bibhtml:
	cd biblio ; bibtex2html -s ../report/naereen.bst -u -charset utf-8 -linebreak *.bib ; cd ..

clean:
	cd report ; make clean ; cd ..
	cd slides ; make clean ; cd ..

stats:
	+git-complete-stats.sh | tee ./complete-stats.txt
	git wdiff ./complete-stats.txt

cloudwords:
	-generate-word-cloud.py -s -m 100 -t "WordCloud - Probabilistic Graphical Models project - (C) 2016 Lilian Besson" ./*.md ./*.txt ./*/*.md ./*/*.tex ./*/*.py
	generate-word-cloud.py -f -o cloudwords.png -m 100 -t "WordCloud - Probabilistic Graphical Models project - (C) 2016 Lilian Besson" ./*.md ./*.txt ./*/*.md ./*/*.tex ./*/*.py
