## About our numerical experiments
> - For more details, see our [final report](../report/).
> - Using [Python (2 or 3)](https://www.python.org/), and [pyhsmm](https://github.com/mattjj/pyhsmm) and [hmmlearn](https://github.com/hmmlearn/hmmlearn).

- [See the animated plots we produced for our experiments](./gif.md)

## Example:
On [2D data drawn from a HSMM](./XZ_from_hsmm__with_hmmlearn.txt), we infer the parameters of a Geometric-HSMM and a Poisson-HSMM, and iteratively during the E-M process we display the most probable clustering given by the HSMM.
The result is an animated gif, displayed below:

### Geometric-HSMM
![Geometric-HSMM on 2D data drawn from a HSMM](./demo_pyhsmm__HDG-HSMM__X_from_hsmm.gif "Geometric-HSMM on 2D data drawn from a HSMM")

### Poisson-HSMM
![Poisson-HSMM on 2D data drawn from a HSMM](./demo_pyhsmm__HDP-HSMM__X_from_hsmm.gif "Poisson-HSMM on 2D data drawn from a HSMM")

----

## [Dependencies](../requirements.txt)
- Usual modules: [numpy](http://numpy.org/), [matplotlib](http://matplotlib.org/), [scikit_learn](http://scikit-learn.org/),
- Less usual modules: [pyhsmm](https://github.com/mattjj/pyhsmm) and [hmmlearn](https://github.com/hmmlearn/hmmlearn).

----

## About
This project was done for the [Probabilistic Graphical Models](http://www.di.ens.fr/~slacoste/teaching/MVA_GM/fall2015/) course for the [MVA master program](http://www.cmla.ens-cachan.fr/version-anglaise/academics/mva-master-degree-227777.kjsp) at [ENS de Cachan](http://www.ens-cachan.fr).

### Copyright
(C), 2015-16, [Lilian Besson](http://perso.crans.org/besson/) et [Valentin Brunck](http://thesilkmonkey.com/science/science.php) ([ENS de Cachan](http://www.ens-cachan.fr)).

### Licence
This project is publicly published, under the terms of the [MIT license](http://lbesson.mit-license.org/).
