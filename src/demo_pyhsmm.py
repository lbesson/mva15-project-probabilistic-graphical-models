#! /usr/bin/env python
# -*- coding: utf-8; mode: python -*-
""" Example of use of pyhsmm.

Reference:
- using pyhsmm, to install with 'pip install pyhsmm',
- https://github.com/mattjj/pyhsmm.


- *Date:* Tuesday 28th of December 2015, 10:55:05.
- *Author:* Lilian Besson, for the MVA Master, (C) 2015-16.
- *Licence:* MIT Licence (http://lbesson.mit-license.org).
"""

from __future__ import print_function, division  # Python 2 compatibility!
import numpy as np
import matplotlib.pyplot as plt
from copy import deepcopy
print("Example of use of HMM and HSMM with Python (2/3) with the pyhsmm module (cf. https://github.com/mattjj/pyhsmm).")

print("Importing pyhsmm...")
try:
    import pyhsmm
    import pyhsmm.basic.distributions as distributions
    from pyhsmm.util.text import progprint_xrange
    from pyhsmm.util.plot import pca_project_data
except ImportError:
    print("FAILED to import pyhsmm. Install it with 'pip install pyhsmm' !")


# Set here what simulation we want to do
hmk3 = False  # Defaut
fromhmm = False
fromhmm_shuffled = False
fromhsmm = True
fromhsmm_shuffled = False


# Get the training and testing data, once and for all
def centering_2D_data(X):
    """ Center and reduce the 2D data in vector X."""
    sigma_x = X[:, 0].std(axis=0)
    mean_x = np.mean(X[:, 0], axis=0)
    sigma_y = X[:, 1].std(axis=0)
    mean_y = np.mean(X[:, 1], axis=0)
    X[:, 0] = (X[:, 0] - mean_x) / sigma_x
    X[:, 1] = (X[:, 1] - mean_y) / sigma_y
    return X


# %% Example 0 : Example of 2D Data:
print("\n\nExample 0 : Example of 2D Data:")
if __name__ == '__main__':
    if hmk3:
        # ext = 'data'
        ext = 'test'
        datatxt = np.loadtxt('EMGaussian.'+ext)
        print("Using the 'EMGaussian."+ext+"' datafile from PGM course Homework 3")
    elif fromhmm:
        datatxt = np.loadtxt('X_from_hmm__with_hmmlearn.txt')
        datatxt = np.loadtxt('XZ_from_hmm__with_hmmlearn.txt')
        print("Using the 'XZ_from_hmm__with_hmmlearn.txt' datafile, generated from a hand-made HMM.")
    elif fromhmm_shuffled:
        datatxt = np.loadtxt('XZ_shuffled_from_hmm__with_hmmlearn.txt')
        print("Using the 'XZ_shuffled_from_hmm__with_hmmlearn.txt' datafile, generated from a hand-made HMM.")
    elif fromhsmm:
        datatxt = np.loadtxt('X_from_hsmm__with_hmmlearn.txt')
        datatxt = np.loadtxt('XZ_from_hsmm__with_hmmlearn.txt')
        print("Using the 'XZ_from_hsmm__with_hmmlearn.txt' datafile, generated from a hand-made HMM.")
    elif fromhsmm_shuffled:
        datatxt = np.loadtxt('XZ_shuffled_from_hsmm__with_hmmlearn.txt')
        print("Using the 'XZ_shuffled_from_hsmm__with_hmmlearn.txt' datafile, generated from a hand-made HMM.")
    else:
        raise ValueError("Unknown datafile.")
    X = datatxt[:, :2]  # We remove the labels
    data = centering_2D_data(X)
    N = np.shape(data)[0]
    plt.figure()
    plt.plot(data[:, 0], data[:, 1], 'kx')
    plt.xlabel("$x$")
    plt.ylabel("$y$")
    plt.axis('tight')
    if hmk3:
        plt.title("2D Data - Mixture of 4 Gaussian ('EMGaussian."+ext+"' from HMK3).")
        plt.savefig("demo_pyhsmm__2D_data__4_Gaussian_X.png")  # XXX
    elif fromhmm:
        plt.title("2D Data ({} points) - From a 4-state HMM \nFile 'XZ_from_hmm__with_hmmlearn.txt', no transition $1 \\leftrightarrow 3$.".format(N))
        plt.savefig("demo_pyhsmm__2D_data__4_Gaussian_X_from_hmm.png")  # XXX
    elif fromhmm_shuffled:
        plt.title("2D Data ({} points) - From a 4-state HMM \nFile 'XZ_shuffled_from_hmm__with_hmmlearn.txt', no transition $1 \\leftrightarrow 3$.".format(N))
        plt.savefig("demo_pyhsmm__2D_data__4_Gaussian_X_shuffled_from_hmm.png")  # XXX
    elif fromhsmm:
        plt.title("2D Data ({} points) - From a 4-state HSMM \nFile 'XZ_from_hsmm__with_hmmlearn.txt', no transition $1 \\leftrightarrow 3$.".format(N))
        plt.savefig("demo_pyhsmm__2D_data__4_Gaussian_X_from_hsmm.png")  # XXX
    elif fromhsmm_shuffled:
        plt.title("2D Data ({} points) - From a 4-state HSMM \nFile 'XZ_shuffled_from_hsmm__with_hmmlearn.txt', no transition $1 \\leftrightarrow 3$.".format(N))
        plt.savefig("demo_pyhsmm__2D_data__4_Gaussian_X_shuffled_from_hsmm.png")  # XXX
    else:
        pass
    plt.show()
    # Second figure
    plt.figure()
    # plt.plot(pca_project_data(data[:100], 2))
    plt.plot(pca_project_data(data, 2))
    plt.legend(["First projection", "Second projection"])
    plt.xlabel("Time")
    plt.ylabel("Value of the PCA projection")
    plt.axis('tight')
    plt.title("2D Data ({} points) - From a 4-state HMM \nPCA projection (1st and 2nd projection) as a function of time".format(N))
    if hmk3:
        plt.savefig("demo_pyhsmm__PCA_projection_function_of_time_X.png")  # XXX
    elif fromhmm:
        plt.savefig("demo_pyhsmm__PCA_projection_function_of_time_X_from_hmm.png")  # XXX
    elif fromhmm_shuffled:
        plt.savefig("demo_pyhsmm__PCA_projection_function_of_time_X_shuffled_from_hmm.png")  # XXX
    elif fromhsmm:
        plt.savefig("demo_pyhsmm__PCA_projection_function_of_time_X_from_hsmm.png")  # XXX
    elif fromhsmm_shuffled:
        plt.savefig("demo_pyhsmm__PCA_projection_function_of_time_X_shuffled_from_hsmm.png")  # XXX
    else:
        pass
    plt.show()


# %% Initialization
n_components = 4

print("\nUsing a Mixture of Gaussian model (from scikit-learn) to initialize mu_0 and sigma_0:")
from sklearn import mixture
gmm_model = mixture.GMM(n_components=n_components,
                        covariance_type='spherical')

print("Fitting this GMM model to our data (asking for {} components).".format(n_components))
gmm_model.fit(data)
print("It produced a GMM model variable (gmm_model) :", gmm_model)
mu_0 = gmm_model.means_
sigma_0 = gmm_model.covars_

print("Evaluation of the score of this GMM (MoG): score_GMM =", np.mean(gmm_model.score(data)))
# -2.36199712151 for 'EMGaussian.test'
# -0.288870915337 for the X,Z data 'XZ_from_hmm__with_hmmlearn.txt'
# -0.288870915337 for the shuffled X,Z data 'XZ_shuffled_from_hmm__with_hmmlearn.txt'
# Of course, MoG/GMM models are PERMUTATION INVARIANT (do not care about order in the data)
# because they model by Gaussian, not by sequences!

# %% Example 1 : A Simple Demonstration:
print("\n\nExample 1 : A Simple Demonstration:")
# https://github.com/mattjj/pyhsmm#a-simple-demonstration

print("\nTo learn an HSMM, we'll use pyhsmm to create a WeakLimitHDPHSMM instance using some reasonable hyperparameters. We'll ask this model to infer the number of states as well, so we'll give it an Nmax parameter:")

obs_dim = 2
Nmax = 4  # XXX it will automatically chose N = K = 4 ?

# Observations hyperparameters
obs_hypparams = {'mu_0': np.zeros(obs_dim),
                 'sigma_0': np.eye(obs_dim),
                 'kappa_0': 0.3,
                 'nu_0': obs_dim+2}


obs_distns = [distributions.Gaussian(**obs_hypparams) for state in range(Nmax)]
# obs_distns = [distributions.Gaussian(**obs_hypparams)]

# XXX impossible to feed it a "good" initialization for mu_0,sigma_0, drat!
# obs_hypparams = {'mu_0': mu_0,
#                  'sigma_0': sigma_0,
#                  'kappa_0': 0.3,
#                  'nu_0': obs_dim+5}


# Durations hyperparameters
dur_hypparams_poisson = {'alpha_0': 2*30,  # XXX what is their meaning
                         'beta_0': 2}  # XXX what is their meaning
# https://en.wikipedia.org/wiki/Poisson_distribution#Bayesian_inference

dur_hypparams_geometric = {'alpha_0': 2*2,  # XXX what is their meaning?
                           'beta_0': 2}  # XXX what is their meaning?
# https://en.wikipedia.org/wiki/Geometric_distribution#Parameter_estimation


# dur_distns = [distributions.PoissonDuration(**dur_hypparams) for state in range(Nmax)]
# https://github.com/mattjj/pybasicbayes/blob/master/pybasicbayes/distributions/poisson.py
dur_distns_poisson = [distributions.PoissonDuration(**dur_hypparams_poisson) for state in range(Nmax)]
# dur_distns_poisson = [distributions.PoissonDuration(**dur_hypparams_poisson)]

# https://github.com/mattjj/pybasicbayes/blob/master/pybasicbayes/distributions/geometric.py
dur_distns_geometric = [distributions.GeometricDuration(**dur_hypparams_geometric) for state in range(Nmax)]
# dur_distns_geometric = [distributions.GeometricDuration(**dur_hypparams_geometric)]


# Posterior models
posteriormodel_poisson = pyhsmm.models.WeakLimitHDPHSMM(
        alpha=6., gamma=6.,  # better to sample over these; see concentration-resampling.py
        init_state_concentration=6.,  # pretty inconsequential
        obs_distns=obs_distns,
        dur_distns=dur_distns_poisson)


posteriormodel_geometric = pyhsmm.models.WeakLimitHDPHSMM(
        alpha=6., gamma=6.,  # better to sample over these; see concentration-resampling.py
        init_state_concentration=6.,  # pretty inconsequential
        obs_distns=obs_distns,
        dur_distns=dur_distns_geometric)


trunc = 60
print("\nWe cut the duration to a max duration Dmax, trunc =", trunc)


print("\nThen, we add the data we want to condition on:")

print("Adding data to posteriormodel_poisson...")
posteriormodel_poisson.add_data(data, trunc=trunc)

print("Adding data to posteriormodel_geometric...")
posteriormodel_geometric.add_data(data, trunc=trunc)


print("\nIf we had multiple observation sequences to learn from, we could add them to the model just by calling add_data() for each observation sequence.")

print("\nNow we run a resampling loop. For each iteration of the loop, all the latent variables of the model will be resampled by Gibbs sampling steps, including the transition matrix, the observation means and covariances, the duration parameters, and the hidden state sequence. We'll also copy some samples so that we can plot them.")


max_idx = 200
step_idx = 10
print("\nStarting {} loops, with a figure every {} resampling.".format(max_idx, step_idx))

# For Poisson durations
print("Starting the loop for the Poisson durations model...")
models_poisson = []
for idx in progprint_xrange(max_idx):
    posteriormodel_poisson.resample_model()
    if (idx+1) % step_idx == 0:
        models_poisson.append(deepcopy(posteriormodel_poisson))


print("Making the plot for Poisson HSMM...")
fig = plt.figure()
for idx, model in enumerate(models_poisson):
    plt.clf()
    model.plot()
    if hmk3:
        plt.gcf().suptitle("Poisson HSMM sampled after %d iterations (HMK3 data)" % (step_idx*(idx+1)))
        plt.savefig("demo_pyhsmm__HDP-HSMM__iter_%.3d.png" % (step_idx*(idx+1)))  # XXX
        print("Saving to 'demo_pyhsmm__HDP-HSMM__iter_%.3d.png'..." % (step_idx*(idx+1)))
    elif fromhmm:
        plt.gcf().suptitle("Poisson HSMM sampled after %d iterations\n (X from a 4-state HMM)" % (step_idx*(idx+1)))
        plt.savefig("demo_pyhsmm__HDP-HSMM__iter_%.3d__X_from_hmm.png" % (step_idx*(idx+1)))  # XXX
        print("Saving to 'demo_pyhsmm__HDP-HSMM__iter_%.3d__X_from_hmm.png'..." % (step_idx*(idx+1)))
    elif fromhmm_shuffled:
        plt.gcf().suptitle("Poisson HSMM sampled after %d iterations\n (X shuffled from a 4-state HMM)" % (step_idx*(idx+1)))
        plt.savefig("demo_pyhsmm__HDP-HSMM__iter_%.3d__X_shuffled_from_hmm.png" % (step_idx*(idx+1)))  # XXX
        print("Saving to 'demo_pyhsmm__HDP-HSMM__iter_%.3d__X_shuffled_from_hmm.png'..." % (step_idx*(idx+1)))
    elif fromhsmm:
        plt.gcf().suptitle("Poisson HSMM sampled after %d iterations\n (X from a 4-state HMM)" % (step_idx*(idx+1)))
        plt.savefig("demo_pyhsmm__HDP-HSMM__iter_%.3d__X_from_hsmm.png" % (step_idx*(idx+1)))  # XXX
        print("Saving to 'demo_pyhsmm__HDP-HSMM__iter_%.3d__X_from_hsmm.png'..." % (step_idx*(idx+1)))
    elif fromhsmm_shuffled:
        plt.gcf().suptitle("Poisson HSMM sampled after %d iterations\n (X shuffled from a 4-state HMM)" % (step_idx*(idx+1)))
        plt.savefig("demo_pyhsmm__HDP-HSMM__iter_%.3d__X_shuffled_from_hsmm.png" % (step_idx*(idx+1)))  # XXX
        print("Saving to 'demo_pyhsmm__HDP-HSMM__iter_%.3d__X_shuffled_from_hsmm.png'..." % (step_idx*(idx+1)))
    else:
        pass
# plt.show()


# For Geometric durations
print("Starting the loop for the Geometric durations model...")
models_geometric = []
for idx in progprint_xrange(max_idx):
    posteriormodel_geometric.resample_model()
    if (idx+1) % step_idx == 0:
        models_geometric.append(deepcopy(posteriormodel_geometric))


print("Making the plot for Geometric HSMM...")
fig = plt.figure()
for idx, model in enumerate(models_geometric):
    plt.clf()
    model.plot()
    if hmk3:
        plt.gcf().suptitle("Geometric HSMM sampled after %d iterations (HMK3 data)" % (step_idx*(idx+1)))
        plt.savefig("demo_pyhsmm__HDG-HSMM__iter_%.3d.png" % (step_idx*(idx+1)))  # XXX
        print("Saving to 'demo_pyhsmm__HDG-HSMM__iter_%.3d.png'..." % (step_idx*(idx+1)))
    elif fromhmm:
        plt.gcf().suptitle("Geometric HSMM sampled after %d iterations\n (X from a 4-state HMM)" % (step_idx*(idx+1)))
        plt.savefig("demo_pyhsmm__HDG-HSMM__iter_%.3d__X_from_hmm.png" % (step_idx*(idx+1)))  # XXX
        print("Saving to 'demo_pyhsmm__HDG-HSMM__iter_%.3d__X_from_hmm.png'..." % (step_idx*(idx+1)))
    elif fromhmm_shuffled:
        plt.gcf().suptitle("Geometric HSMM sampled after %d iterations\n (shuffled X from a 4-state HMM)" % (step_idx*(idx+1)))
        plt.savefig("demo_pyhsmm__HDG-HSMM__iter_%.3d__X_shuffled_from_hmm.png" % (step_idx*(idx+1)))  # XXX
        print("Saving to 'demo_pyhsmm__HDG-HSMM__iter_%.3d__X_shuffled_from_hmm.png'..." % (step_idx*(idx+1)))
    elif fromhsmm:
        plt.gcf().suptitle("Geometric HSMM sampled after %d iterations\n (X from a 4-state HMM)" % (step_idx*(idx+1)))
        plt.savefig("demo_pyhsmm__HDG-HSMM__iter_%.3d__X_from_hsmm.png" % (step_idx*(idx+1)))  # XXX
        print("Saving to 'demo_pyhsmm__HDG-HSMM__iter_%.3d__X_from_hsmm.png'..." % (step_idx*(idx+1)))
    elif fromhsmm_shuffled:
        plt.gcf().suptitle("Geometric HSMM sampled after %d iterations\n (shuffled X from a 4-state HMM)" % (step_idx*(idx+1)))
        plt.savefig("demo_pyhsmm__HDG-HSMM__iter_%.3d__X_shuffled_from_hsmm.png" % (step_idx*(idx+1)))  # XXX
        print("Saving to 'demo_pyhsmm__HDG-HSMM__iter_%.3d__X_shuffled_from_hsmm.png'..." % (step_idx*(idx+1)))
    else:
        pass
# plt.show()


# End of example_pyhsmm.py
