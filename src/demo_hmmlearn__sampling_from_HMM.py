#! /usr/bin/env python
# -*- coding: utf-8; mode: python -*-
"""
Sampling from HMM
-----------------

This script shows how to sample points from a Hiden Markov Model (HMM):
we use a 4-components with specified mean and covariance.

The plot show the sequence of observations generated with the transitions
between them. We can see that, as specified by our transition matrix,
there are no transition between component 1 and 3.

It saves the *ordered* data to 'X_from_hmm__with_hmmlearn.txt' for 2D points,
and 'Z_from_hmm__with_hmmlearn.txt' for the "true" labels.
WARNING: the order of this X is important!


Reference:
- using hmmlearn, to install with 'pip install hmmlearn',
- https://hmmlearn.readthedocs.org/en/latest/tutorial.html.

Source: https://hmmlearn.readthedocs.org/en/latest/_downloads/plot_hmm_sampling.py
"""
from __future__ import print_function, division  # Python 2 compatibility!

import numpy as np
import matplotlib.pyplot as plt

print("Importing hmm from hmmlearn...")
try:
    import hmmlearn
    from hmmlearn import hmm
except ImportError:
    print("FAILED to import hmmlearn. Install it with 'pip install hmmlearn' !")

print(__doc__)
# np.random.seed(42)  # XXX No random!


##############################################################
# Prepare parameters for a 4-components HMM

# Initial population probability,
# starting from 1 at 50%, 2 and 4 equally at 25%
startprob = np.array([0.5, 0.25, 0.0, 0.25])

# The transition matrix, note that there are no transitions
# possible between component 1 and 3
transmat = np.array([[0.95, 0.025, 0.0, 0.025],
                     [0.025, 0.95, 0.025, 0.0],
                     [0.0, 0.025, 0.95, 0.025],
                     [0.025, 0.0, 0.025, 0.95]])
# The transition matrix A is ~= almost diagonal => long sequences !

meanDuration = int(np.ceil(1.0 / (1.0 - np.mean(np.diag(transmat)))))
# Should be 50

# The means of each component
means = np.array([[0.0,   0.0],  # Cluster 1 and 3 are very similar !!
                  [0.0,  10.0],
                  [0.0,   0.0],   # Cluster 1 and 3 are very similar !!
                  [10.0,  0.0]])

# The covariance of each component
covars = .5 * np.tile(np.identity(2), (4, 1, 1))

# Build an HMM instance and set parameters
model = hmm.GaussianHMM(n_components=4, covariance_type='full')

# Instead of fitting it from the data, we directly set the estimated
# parameters, the means and covariance of the components
model.startprob_ = startprob
model.transmat_ = transmat
model.means_ = means
model.covars_ = covars


# Generate samples
nbSample = 1000
X, Z = model.sample(nbSample)
data = np.hstack((X, Z.reshape((nbSample, 1))))

colors = ['k', 'r', 'y', 'c']  # TODO: change from K=4 to generic K
markers = ['o', 's', '*', 'd']  # TODO: change from K=4 to generic K


if __name__ == '__main__':
    # Plot the sampled data
    plt.hold(True)
    # Points and line between then (ordering, showing transition)
    plt.plot(X[:, 0], X[:, 1], 'b.-', label='Transition between observations', ms=1, alpha=0.4)
    for q in range(4):
        xq = X[Z == q]
        cq = colors[q]
        mq = markers[q]
        plt.plot(xq[:, 0], xq[:, 1], cq+mq, ms=8, alpha=0.8,  # cq+mq,
                 color=cq, marker=mq, linestyle='',
                 label='Cluster %i (mean %i,%i)' % ((q + 1), int(means[q][0]), int(means[q][1])))
    # plt.scatter(X[:, 0], X[:, 1], c=Z, alpha=0.8, s=50)
    plt.hold(False)
    plt.title("%i points drawn from a 2D 4-state HSMM, with mean duration $%i$.\nNo transition from overlapping clusters $1 \\leftrightarrow 3$" % (nbSample, meanDuration))

    # Indicate the component numbers
    for q, m in enumerate(means):
        if q == 0:
            plt.text(m[0]-1, m[1]+2, 'Cluster %i' % (q + 1),
                     size=17, horizontalalignment='center',
                     bbox=dict(alpha=0.5, facecolor='w'))
        elif q == 2:
            plt.text(m[0]+1, m[1]-2, 'Cluster %i' % (q + 1),
                     size=17, horizontalalignment='center',
                     bbox=dict(alpha=0.5, facecolor='w'))
        else:
            plt.text(m[0], m[1], 'Cluster %i' % (q + 1),
                     size=17, horizontalalignment='center',
                     bbox=dict(alpha=0.5, facecolor='w'))
    plt.legend(loc='best')
    # plt.axis('equal')
    plt.savefig('X_transition_Z_from_hsmm__with_hmmlearn.png')
    plt.show()


np.savetxt('X_from_hsmm__with_hmmlearn.txt', X)
np.savetxt('Z_from_hsmm__with_hmmlearn.txt', Z, fmt='%i')

np.savetxt('XZ_from_hsmm__with_hmmlearn.txt', data)


# We loose the sequential aspect!
np.random.shuffle(data)
np.savetxt('XZ_shuffled_from_hsmm__with_hmmlearn.txt', data)
