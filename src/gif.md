## About our numerical experiments
> - For more details, see our [final report](../report/).
> - Using Python (2 or 3), and [pyhsmm](https://github.com/mattjj/pyhsmm) and [hmmlearn](https://github.com/hmmlearn/hmmlearn).

Below is displayed all the animated plots we produced for our experiments:

## On [HMK3 data](./EMGaussian.test)
### Geometric-HSMM
![Geometric-HSMM on HMK3 data](./demo_pyhsmm__HDG-HSMM.gif "Geometric-HSMM on HMK3 data")

### Poisson-HSMM
![Poisson-HSMM on HMK3 data](./demo_pyhsmm__HDP-HSMM.gif "Poisson-HSMM on HMK3 data")


----

## On [2D data drawn from a HMM](./XZ_from_hmm__with_hmmlearn.txt)
### Geometric-HSMM
![Geometric-HSMM on 2D data drawn from a HMM](./demo_pyhsmm__HDG-HSMM__X_from_hmm.gif "Geometric-HSMM on 2D data drawn from a HMM")

### Poisson-HSMM
![Poisson-HSMM on 2D data drawn from a HMM](./demo_pyhsmm__HDP-HSMM__X_from_hmm.gif "Poisson-HSMM on 2D data drawn from a HMM")

## On shuffled [2D data drawn from a HMM](./XZ_from_hmm__with_hmmlearn.txt)
### Geometric-HSMM
![Geometric-HSMM on shuffled 2D data drawn from a HMM](./demo_pyhsmm__HDG-HSMM__X_shuffled_from_hmm.gif "Geometric-HSMM on shuffled 2D data drawn from a HMM")

### Poisson-HSMM
![Poisson-HSMM on shuffled 2D data drawn from a HMM](./demo_pyhsmm__HDP-HSMM__X_shuffled_from_hmm.gif "Poisson-HSMM on shuffled 2D data drawn from a HMM")

----

## On [2D data drawn from a HSMM](./XZ_from_hsmm__with_hmmlearn.txt)
### Geometric-HSMM
![Geometric-HSMM on 2D data drawn from a HSMM](./demo_pyhsmm__HDG-HSMM__X_from_hsmm.gif "Geometric-HSMM on 2D data drawn from a HSMM")

### Poisson-HSMM
![Poisson-HSMM on 2D data drawn from a HSMM](./demo_pyhsmm__HDP-HSMM__X_from_hsmm.gif "Poisson-HSMM on 2D data drawn from a HSMM")

----

## About
This project was done for the [Probabilistic Graphical Models](http://www.di.ens.fr/~slacoste/teaching/MVA_GM/fall2015/) course for the [MVA master program](http://www.cmla.ens-cachan.fr/version-anglaise/academics/mva-master-degree-227777.kjsp) at [ENS de Cachan](http://www.ens-cachan.fr).

### Copyright
(C), 2015-16, [Lilian Besson](http://perso.crans.org/besson/) et [Valentin Brunck](http://thesilkmonkey.com/science/science.php) ([ENS de Cachan](http://www.ens-cachan.fr)).

### Licence
This project is publicly published, under the terms of the [MIT license](http://lbesson.mit-license.org/).
