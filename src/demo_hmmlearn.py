#! /usr/bin/env python
# -*- coding: utf-8; mode: python -*-
""" Example of use of hmmlearn.

Reference:
- using hmmlearn, to install with 'pip install hmmlearn',
- https://hmmlearn.readthedocs.org/en/latest/tutorial.html.


- *Date:* Monday 28th of December 2015, 17:58:05.
- *Author:* Lilian Besson, for the MVA Master, (C) 2015-16.
- *Licence:* MIT Licence (http://lbesson.mit-license.org).
"""

from __future__ import print_function, division  # Python 2 compatibility!
print("Example of use of HMM with Python (2/3) with the hmmlearn module (cf. http://hmmlearn.rtfd.org/).")
import numpy as np
import matplotlib.pyplot as plt

print("Importing hmm from hmmlearn...")
try:
    import hmmlearn
    from hmmlearn import hmm
except ImportError:
    print("FAILED to import hmmlearn. Install it with 'pip install hmmlearn' !")


# %% Example 1 : Building HMM and generating samples:
print("\nExample 1 : Building HMM and generating samples:")
# https://hmmlearn.readthedocs.org/en/latest/tutorial.html#building-hmm-and-generating-samples

np.random.seed(42)  # XXX right now the final plot is not generic, do not remove this!

print("You can build an HMM instance by passing the parameters described above to the constructor. Then, you can generate samples from the HMM by calling sample.")

print("Creating a HMM with Gaussian probability emission (full covariance, 3 components)...")
model = hmm.GaussianHMM(n_components=3, covariance_type='full')
model.startprob_ = np.array([0.6, 0.3, 0.1])
model.transmat_  = np.array([[0.7, 0.2, 0.1],
                             [0.3, 0.5, 0.2],
                             [0.3, 0.3, 0.4]])
model.means_  = np.array([[0.0, 0.0], [3.0, -3.0], [5.0, 10.0]])
model.covars_ = np.tile(np.identity(2), (3, 1, 1))
print("model:\n", model)

nbSample = 200
print("Generating {} sample from this first HMM:".format(nbSample))
X, Z = model.sample(nbSample)

print("X = Observed data :")
print(X[:10])
print("Z = Hidden states :")
print(Z[:10])


# Left-right model
print("Creating a HMM with Gaussian probability emission and non-ergodic Markov-chain...")
print("The transition probability matrix need not to be ergodic. For instance, a left-right HMM can be defined as follows.")
lr = hmm.GaussianHMM(n_components=3, covariance_type='diag',
                     init_params='cm', params='cmt')
lr.startprob_ = np.array([1.0, 0.0, 0.0])
lr.transmat_  = np.array([[0.5, 0.5, 0.0],
                          [0.0, 0.5, 0.5],
                          [0.0, 0.0, 1.0]])
print("lr:\n", lr)

# print("Generating 100 sample from this second HMM:")
# X, Z = lr.sample(100)

# print("X = Observed data :")
# print(X[:10])
# print("Z = 'True' hidden states :")
# print(Z[:10])


# %% Example 2 : Learning parameters (EM)
# Training HMM parameters and inferring the hidden states:
print("\nExample 2 : Training HMM parameters and inferring the hidden states:")
# https://hmmlearn.readthedocs.org/en/latest/tutorial.html#training-hmm-parameters-and-inferring-the-hidden-states

remodel = hmm.GaussianHMM(n_components=3, covariance_type='full', n_iter=100)

full_X = np.hstack((X, Z.reshape((nbSample, 1))))
# remodel.fit([full_X])  # XXX

np.random.seed(42)  # XXX right now the final plot is not generic, do not remove this!
remodel.fit([X])  # XXX
print("remodel:\n", remodel)

np.random.seed(42)  # XXX right now the final plot is not generic, do not remove this!
Z2 = remodel.predict(X)
print("Z2 = Predicted hidden states :")
print(Z2[:10])

# Plot the sampled data
if __name__ == '__main__':
    plt.figure()
    plt.subplot(1, 2, 1)
    plt.scatter(X[:, 0], X[:, 1], c=Z, s=40, marker='o', alpha=0.85)
    plt.title("'True' clustering (used for sampling random test data).")
    plt.subplot(1, 2, 2)
    plt.scatter(X[:, 0], X[:, 1], c=Z2, s=40, marker='o', alpha=0.85)
    Z3 = Z2.copy()
    # Permutation to make it have the same convention as to Z
    # FIXME not generic, do not remove the np.random.seed(42)
    Z2[Z2 == 2] = 3
    Z2[Z2 == 1] = 2
    Z2[Z2 == 3] = 1
    indeces = (Z != Z2)
    Xdiff = X[indeces]
    rateMistake = np.shape(Xdiff)[0] / np.shape(X)[0]
    plt.scatter(Xdiff[:, 0], Xdiff[:, 1], c='k', s=250, marker='*', alpha=0.7)
    plt.title("Predicted clustering (Viterbi algorithm for HMM) : {:.2%} mistake.".format(rateMistake))
    plt.show()



print("""
# TODO :
- generate data from a random HMM, save it to txt files,
- then use our HMM (and HSMM) implementation to check that we can learn the good pi,A and mus,Sigmas,
- use our HMM to check that the inferred hidden states Z_own is close to « the true » Z.
    """)


print("TODO: finish this script example_hmmlearn.py!")
# End of example_hmmlearn.py
