#! /usr/bin/env python3
# -*- coding: utf-8; mode: python -*-
""" Implementation of Hidden semi-Markov Models, for our Project of the PGM course (Master MVA).

We already did:
- (OK) alpha/beta recursion (forward/backward propagation),
- (OK) test alpha/beta recursion ? (especially beta, not sure of the code),
- (OK) generalize the alpha/beta for any distribution of durations (given fn_log_D_j_d),
- (OK) test the generalized alpha/beta for any distribution of durations,
- (OK) likelihood and log-likelihood computations,

We could have to do:
- (TODO) EM algorithm for HSMM,
- (TODO) an online EM algorithm for HSMM (see [Bach15]),
- (TODO) Comparison to (simple) HMM on toy data (2D Gaussian),
- (TODO) Viterbi algorithm for HSMM,
- (TODO) use it on real data ? (not necessary, probably too long).


This HSMM.py program requires other programs:

- EM.py for the Estimation-Maximisation algorithm for the Mixture of Gaussian model,
- kmeans.py is required by EM.py, not by HMM.py.
- HMM.py for comparison and function already defined there (logsumexp and utility functions for gaussian densities like log_gaussian_density,...)

Reference:

- [Murphy2002]: Hidden Semi-Markov Models (HSMMs), by Kevin Murphy (2002), http://www.cs.ubc.ca/~murphyk/Papers/segment.pdf.


- *Date:* Wednesday 13 January 2016.
- *Author:* Lilian Besson and Valentin Brunck, for the MVA Master, (C) 2015-16.
- *Licence:* MIT Licence (http://lbesson.mit-license.org).
"""

from __future__ import print_function, division  # Python 2 compatibility

# XXX if needed, uncomment here to load more modules
from os.path import join as ospathjoin
import numpy as np
import numpy.linalg as la
import matplotlib.pyplot as plt

# From our modules
import EM  # Local import
# from EM import EM_general  # Local import
# import HMM  # Local import
# from HMM import log_gaussian_density  # Local import
from EM import EM_general  # Local import

# Option for customizing some part of the code
doSaveThePlots = True  # True to save the figures
saveAlsoPDF    = False  # Save also to PDF (default is just PNG)


try:
    from scipy.misc import logsumexp  # log trick (to reduce underflow risks)
except ImportError:
    def logsumexp(li):
        """ Our own log-sum-exp function (**naive**, not optimized)."""
        return np.log(np.sum(np.exp(li)))
    print("We defined our own logsumexp function, scipy.misc.logsumexp should be use instead!")

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# %% Utility functions on Gaussian densities


def gaussian_density(xi, muj, Sigmaj, d=None):
    """ N(x_i | mu_j, Sigma_j) : density of a Gaussian of center muj and covariance matrix Sigmaj."""
    if d is None:
        d = np.shape(xi)[0]
    xc = xi - muj
    return np.exp(- (np.dot(xc.T, np.dot(la.inv(Sigmaj), xc))) / 2) / np.sqrt(la.det(Sigmaj) * (2*np.pi)**d)


def log_gaussian_density(xi, muj, Sigmaj, d=None):
    """ Log normale density for xi, from parameters mu_j Sigma_j (log N(x_i | mu_j, Sigma_j)):

    log density = -1/2 d log(2 pi) -1/2 x^T . S . x -1/2 log det(S)
    """
    if d is None:
        d = np.shape(xi)[0]
    xc = xi - muj  # xi centered
    log_px = - 0.5 * d * np.log(2*np.pi)  # -1/2 d log(2 pi)
    log_px -= 0.5 * np.dot(xc.T, np.dot(la.inv(Sigmaj), xc))  # -1/2 x^T . S . x
    log_px -= 0.5 * np.log(la.det(Sigmaj))  # -1/2 log det(S)
    return log_px

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# %% 1. alpha/beta recursion
print("\n\n1. alpha/beta recursion for HSMM :")
print("Reference :\n - http://www.di.ens.fr/~slacoste/teaching/MVA_GM/fall2015/index_projects.html")


def alpha_recursion(u, A, mus, Sigmas, pi_init, fn_log_D_j_d, d_max):
    """ Alpha recursion for HSMM, working only with log for improved numerical stability (no underflow risk).

    - The argument fn_log_D_j_d has to be chosen by the user, as a function j, d -> log_D_j_d (see Murphy2002 for the notations, e.g. geometric to emulate a HMM),
    - Complexity: O(T L K), for the notations cf. [Murphy2002] § 1.2 page 6,
    - Return: log_alpha, vector of log values of alpha.
    """
    # 0. Creating variables
    T = np.shape(u)[0]    # Number of points and dimension
    K = np.shape(mus)[0]  # Number of clusters
    log_alpha = np.zeros((T, K))  # Goal
    # 1. Initialization of alpha
    for k in range(K):
        mu_k = mus[k, :]
        sigma_k = Sigmas[k]
        log_alpha[0, k] = log_gaussian_density(u[0, :], mu_k, sigma_k) + np.log(pi_init[k])
    # 2. Forward pass
    for t in range(1,T):  # O(T)
        for j in range(K):  # O(K)
            mu_j = mus[j, :]
            sigma_j = Sigmas[j]
            # 2.a Compute log_alpha_star
            log_alpha_star = np.zeros((T, K))  # Temporary variable
            for d in range(t,-1,-1):
                a = np.log(A[:, j]) + log_alpha[t-d, :].T
                max_a = np.max(a)
                log_alpha_star[t-d, j] = (max_a + logsumexp(a - max_a))*1
            # 2.b Compute log_alpha
            b = np.empty(t)
            b.fill(-1 * np.inf)
            for d in range(0,t):  # The O(L) comes from here
                # stopping for d>d_max makes the algo fast:
                if d >= d_max:
                    break
                else:
                    log_B_t_j_d = 0  # Sum for s = t-d+1 .. t
                    for s in range(t-d+1, t+1):
                        log_B_t_j_d += log_gaussian_density(u[s, :], mu_j, sigma_j)
                    log_D_j_d = fn_log_D_j_d(j, d)
                    b[d] = log_B_t_j_d + log_D_j_d + log_alpha_star[t-d, j]
            max_b = np.max(b)
            log_alpha[t, j] = max_b + logsumexp(b - max_b)
    # 3. Done for log-alpha recursion
    print("done for alpha recursion")
    return log_alpha


def beta_recursion(u, A, mus, Sigmas, fn_log_D_j_d, d_max):
    """ Beta recursion for HSMM, working only with log for improved numerical stability (no underflow risk).

    - The argument fn_log_D_j_d has to be chosen by the user, as a function j, d -> log_D_j_d (see Murphy2002 for the notations, e.g. geometric to emulate a HMM),
    - Complexity: O(T L K^2), for the notations cf. [Murphy2002] § 1.2 page 5-6,
    - Return: log_beta, vector of log values of beta.
    """
    # 0. Creating variables
    T = np.shape(u)[0]    # Number of points and dimension
    K = np.shape(mus)[0]  # Number of clusters
    # 1. Initialization for beta : Beta_T(z_T) = 1
    log_beta = np.zeros((T, K))
    # 2. Backward pass
    for t in range(T-2, -1, -1):  # O(T)
        for i in range(K):  # O(K)
            # 2.a Compute log_beta_star
            log_beta_star = np.zeros(K)  # Temporary variable
            for j in range(K):  # O(K)
                mu_j = mus[j, :]
                sigma_j = Sigmas[j]
                a = np.empty(T)
                a.fill(-1 * np.inf)  # Temporary variable

                # XXX OK duration d has to be > 0 !
                for d in range(1, T-t):  # The O(L) comes from here
                    if d >= d_max:
                        break
                    else:
                        log_B_t_plus_d_j_d = 0  # Sum s = t+1 .. t+d
                        for s in range(t+1, t+d+1):
                            log_B_t_plus_d_j_d += log_gaussian_density(u[s, :], mu_j, sigma_j)
                        log_D_j_d = fn_log_D_j_d(j, d)
                        a[d] = log_B_t_plus_d_j_d + log_D_j_d + log_beta[t+d, j]
                max_a = np.max(a)
                log_beta_star[j] = max_a + logsumexp(a - max_a)
            # 2.b Compute log_beta
            b = np.log(A[i, :]) + log_beta_star
            max_b = np.max(b)
            log_beta[t, i] = max_b + logsumexp(b - max_b)
    # 3. Done for log-beta recursion
    print ("done dor beta recursion")
    return log_beta


def alphabeta(u, A, mus, Sigmas, pi_init, fn_log_D_j_d, d_max):
    """ alpha/beta recursion for HSMM (forward-backward algorithm, aka sum-product), with the log-trick to reduce underflow risk.

    - The argument fn_log_D_j_d has to be chosen by the user, as a function j, d -> log_D_j_d (see Murphy2002 for the notations, e.g. geometric to emulate a HMM),
    - Return: log_alpha, log_beta,
    - Complexity: O(T L K^2) (from the beta recursion),
    - Reference: [Murphy2002], § 1.4.5 page 10.
    """
    log_alpha = alpha_recursion(u, A, mus, Sigmas, pi_init, fn_log_D_j_d, d_max)
    log_beta = beta_recursion(u, A, mus, Sigmas, fn_log_D_j_d, d_max)
    return log_alpha, log_beta


def alphabeta_geometric(u, A, mus, Sigmas, pi_init):
    """ alpha/beta recursion for HSMM, with the log-trick to reduce underflow risk.

    - It uses the special case geometric distribution of durations (given by geometric_duration), in order to emulate an HMM,
    - Return: log_alpha, log_beta,
    - Complexity: O(T L K^2) (from the beta recursion),
    - Reference: [Murphy2002], § 1.4.5 page 10.
    """
    fn_log_D_j_d = geometric_duration(A)
    d_max = 2
    return alphabeta(u, A, mus, Sigmas, pi_init, fn_log_D_j_d, d_max)


def geometric_duration(A):
    """ A geometric distribution of durations, when the transition of the Markov Chain are homogeneous, given by the square matrix A and d.

    - Return: fn_log_D_j_d a function of j, d -> log_D_j_d,
    - It is used for alphabeta_geometric, in order to emulate a HMM with a HSMM,
    - Reference: [Murphy2002], § 1.4.5 page 10.
    """
    # This part is exactly the log of a geometric distribution
    # log( p . (1-p)**d ) = log( p ) + d * log(1-p)
    # (p = 1 - A[j,j] = proba of leaving current state j)
    def fn_log_D_j_d(j, d):
        return np.log(1-A[j, j]) + d*np.log(A[j, j])
    return fn_log_D_j_d


alphabeta_HMMlike = alphabeta_geometric


def compute_filtering(log_alpha, log_beta, K, t_max):
    """ Use the values of alpha and beta

    - Return: the marginal distribution of hidden states p(q_t | u1,..,uT),
    - Note: it is called *filtering* because this operation comes from denoising sound,
    - Reference: Bishop, Pattern Recognition and Machine Learning, § 13.2.2 (page 618).
    """
    filtering = np.zeros((t_max+1, K))
    for t in range(t_max+1):
        ai = log_alpha[t, :] + log_beta[t, :]
        max_ai = np.max(ai)
        log_normalization = max_ai + logsumexp(log_alpha[t, :] + log_beta[t, :] - max_ai)
        filtering[t, :] = np.exp(log_alpha[t, :] + log_beta[t, :] - log_normalization)
        filtering[t,:] /= np.sum(filtering[t,:])
    return filtering


def filtering_plot(u, K=4, t_max=100, save=doSaveThePlots):
    """ Filtering plots: K plots, clusterization from an untrained HSMM."""
    # Using the means and covariances obtained from MoG from HWK2
    mus, Sigmas, _ = EM_general(u, K)
    # Initial pi (uniform), A estimations :
    # Aii = 1/2, Aij = 1/6 off-diagonal
    pi_init = np.ones(K) / K
    A = np.ones((K, K)) / (2*(K-1)) + np.eye(K)*(0.5 - 1/(2*(K-1)))
    print("With these naive pi and A, compute p(q_t | u1,..,uT) (filtering) with T=500 and t:1,100...")
    # Compute alpha_t and beta_t from all of the 500 test points
    print("computing alpha-beta:")
    log_alpha, log_beta = alphabeta_HMMlike(u, A, mus, Sigmas, pi_init)
    print(log_alpha,log_beta)
    # p(q_t | u1,..,uT) (filtering) with T=500 and t:1,100
    print("computing filtering:")
    filtering = compute_filtering(log_alpha, log_beta, K, t_max)

    # Plot : 4 subplots, for 100 first points
    plt.figure()
    colors = ['k', 'r', 'y', 'c']  # TODO: change from K=4 to generic K
    markers = ['o', 's', '*', 'd']  # TODO: change from K=4 to generic K
    for q in range(K):
        cq = colors[q]
        mq = markers[q]
        plt.subplot(K, 1, q+1)
        plt.plot(filtering[:, q], color=cq, marker=mq, linestyle='-')
        # plt.plot(filtering[:, q], 'b+-')
        plt.xlim([0, t_max+1])
        plt.xlabel("$t$")
        plt.ylim([0, 1.1])
        plt.ylabel(r"$\mathbb{P}(q_t = %i | u_{1..%i})$" % (q+1, t_max))
        if q == 0:
            plt.title("Filtering HSMM")
        else:
            plt.title("For state $q = {}$".format(q+1))
    print("Plotting the filtering p(q_t|u_1,..,u_T) for each state (q=1,..,{}) for the first {} points".format(K, t_max))
    if save:
        name = "filtering_dataHWK2__tmax_{}".format(t_max)
        print("Saving this figure to: fig/{}.png ...".format(name))
        plt.savefig(ospathjoin("fig", name + ".png"))
        if saveAlsoPDF:
            print("Saving this figure to: fig/{}.png ...".format(name))
            plt.savefig(ospathjoin("fig", name + ".pdf"))
    return filtering


print("\nDone for HSMM.py")
# End of HSMM.py
