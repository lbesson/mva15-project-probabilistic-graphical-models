#! /usr/bin/env python3
# -*- coding: utf-8; mode: python -*-
""" Implementation of the K-means algorithm, 3 different initializations.

Homework assignement was on http://www.di.ens.fr/~slacoste/teaching/MVA_GM/fall2015/Exercises_11nov2015.pdf
With data from http://www.di.ens.fr/~slacoste/teaching/MVA_GM/fall2015/classification_data_HWK2.zip

- *Date:* Wednesday 11 November 2015.
- *Author:* Lilian Besson and R.Ritavan, for the MVA Master, (C) 2015-16.
- *Licence:* MIT Licence (http://lbesson.mit-license.org).
"""

from __future__ import print_function, division  # Python 2 compatibility

import numpy as np
import numpy.linalg as la
import matplotlib.pyplot as plt

from os.path import join as ospathjoin
# from sklearn import cluster


# %% 3 initializations

def random_initialiazation(X, n_clusters, gaussian=False):
    """ Random initialization for k-mean, two methods:

    - gaussian = False: just k random points in the dataset.
    - gaussian = True: by taking k centroids from a (multi-variate) normal distribution of mean and variance estimated from the data.
    """
    if gaussian:
        # First choice (random point on a Gaussian fitting our dataset):
        meanX, covX = np.mean(X, 0), np.cov(X.T)
        return np.random.multivariate_normal(meanX, covX, n_clusters)
    else:
        # Second choice (random point in our dataset):
        inds = np.random.choice(range(np.size(X, 0)), n_clusters, replace=False)
        return X[inds]


def k_means_plusplus(X, n_clusters):
    """ Selects initial cluster centers for k-mean clustering in a smart way to speed up convergence.

    Ref: http://ilpubs.stanford.edu:8090/778/1/2006-13.pdf (Algo 2.2).
    """
    n, d = np.shape(X)
    mu = np.zeros((n_clusters, d))
    D = np.zeros(n)
    # Step 0.a, take one center c1, chosen uniformly at random from X
    mu[0] = X[np.random.choice(range(n), 1)]  # random.choice(X) works also
    # Step 0.b, take a new center ci, choosing x in X randomly with "D^2 weighted" probability
    for t in range(1, n_clusters):
        # 0.b.i) compute D(x) for each x
        for i in range(n):
            label_i = np.argmin(la.norm(X[i] - mu[0:t], axis=1))
            D[i] = la.norm(X[i] - mu[label_i])**2
        D /= np.sum(D)  # Make it a proba distribution
        # 0.b.ii) take the next center at random but with this weighted probability
        mu[t] = X[np.random.choice(range(n), 1, p=D)]
        # We use the p parameter from np.random.choice
        # p : 1-D array-like, optional: The probabilities associated with each entry in a.
    # Step 0.c, repeat step 0.b until we have enough clusters
    return mu


# %% Compute the distortion
def distortion(X, mu, labels, K):
    r""" The distortion (the cost function we try to minimize it), for the dataset X, is defined as:

    J(mu, z) = distortion(X, mu, z) = -sum_i^n sum_k^K z_i^k ||x_i - \mu_k||^2
    """
    return np.sum(la.norm(X[labels == k] - mu[k])**2 for k in range(K))


# %% K-means
def k_means(X, n_clusters, n_init=10, max_iter=50, tol=1e-3,
            init='k-means++', use_sklearn=False, verbose=False):
    """ The K-means clustering algorithm.

    References:
        - http://www.di.ens.fr/~fbach/courses/fall2013/lecture3.pdf#page=1 (§3.1)
        - "k-means++: The advantages of careful seeding" Arthur, David, and Sergei Vassilvitskii (http://ilpubs.stanford.edu:8090/778/1/2006-13.pdf)

    Parameters:
        - X (array-like, shape (n, d)):
            The datapoint to cluster.
        - n_clusters (int):
            The number of clusters to form as well as the number of centroids to generate (has to be provided).
        - max_iter (int):
            Maximum number of iterations of the k-means algorithm to run.
        - n_init (int):
            Number of time the k-means algorithm will be run with different centroid seeds.
            The final results will be the best output of n_init consecutive runs in terms of distortion.
        - init ('k-means++', 'gaussian' or 'random'):
            How to choose the initial centroids.
            K-means++ is more efficient but more complex.
        - use_sklearn (boolean):
            Use `sklearn.cluster.k_means` instead of our implementation.

    Returns:
        - centroids (float ndarray with shape (k, d)):
            Centroids found by the k-means algorithm (at the end)

        - labels (integer ndarray with shape (n,)):
            label[i] is the index of the centroid the i'th observation is closest to (between 0 and n_clusters-1).

        - distortionJ (float):
            The final value of the distortion criterion (sum of squared distances to the closest centroid for all observations in the training set).
        - mu0 (float ndarray with shape (k, d)):
            The initial centroids for the best run.
    """
    # Using Scikit-Learn's implementation (to compare)
    if use_sklearn:
        from sklearn import cluster
        return cluster.k_means(X, n_clusters, init=init, n_init=n_init, max_iter=max_iter, tol=tol, verbose=verbose)
    def log(*args):
        if verbose:
            print(args)
    # Otherwise that's our implementation
    n, _   = np.shape(X)  # nb of points, nb of features
    best_J = np.infty     # The best distortion we found so far
    # We try n_init times, keeping the best result
    for tentative in range(n_init):
        log("Starting the", tentative, "th tentative...")
        # Step 0 : Get the initial centroids
        if init == 'random':
            mu = random_initialiazation(X, n_clusters, gaussian=False)
        elif init == 'gaussian':
            mu = random_initialiazation(X, n_clusters, gaussian=True)
        elif init == 'k-means++':
            mu = k_means_plusplus(X, n_clusters)
        else:
            raise ValueError("k_means(): unknown value for 'init' parameter, only 'random', 'gaussian' and 'k-means++' are available.")
        log("Initialization completed with method {}.".format(init))
        mu0 = mu
        t = 0
        labels = np.zeros(n)  # Random initial labels, useless
        J = distortion(X, mu, labels, n_clusters)  # Bad initial distortion
        # Start the loop
        for t in range(max_iter):
            # print("Step", t, ", with distortion J =", J)  # DEBUG
            # Step 1 : xi gets associated to the closer centroid
            for i in range(n):  # This loop can be slow, FIXME
                labels[i] = np.argmin(la.norm(X[i] - mu, axis=1))
            # Step 2 : mu becomes the mean of its points
            for k in range(n_clusters):
                mu[k] = np.mean(X[labels == k], axis=0)
            # Evaluate the new distortion, stop if it has not changed enough
            J_next = distortion(X, mu, labels, n_clusters)
            if abs(J - J_next) < tol:
                log("Distortion J = {:g}, it has stopped decreasing (with tol = {:g}), so we stop now ({} iterations).".format(J_next, tol, t))
                break
            J = J_next
        # If this tentative is the best one so far:
        if J < best_J:
            log("This tentative improved the best distortion we had (currently J = {}).".format(J))
            best_J = J
            best_mu = mu
            best_labels = labels
            best_mu0 = mu0
        # Keep trying
    return best_mu, best_labels, best_J, best_mu0
# End of k_means


# %% Demo of K-means (general function)
def demo_kmeans(X, n_clusters=4, init='k-means++', n_init=1,
                print_distortion_measures=True, use_sklearn=False,
                doSaveThePlots=False, saveAlsoPDF=False):
    """ Represent graphically the data X, the cluster centers, as well as the different clusters.
    """
    # First get the centroids from training data
    if use_sklearn:
        centroids, labels, J = k_means(X, n_clusters, init=init, n_init=n_init, use_sklearn=use_sklearn)
    else:
        centroids, labels, J, mu0 = k_means(X, n_clusters, init=init, n_init=n_init, use_sklearn=use_sklearn)
    # Then we plot
    print("Plotting the training data from 'EMGaussian.train' ({} points) with centroids and clusters...".format(X.shape[0]))
    print("{} {}-means, distortion {:g}, init {}, n_init {}".format("sklearn's" if use_sklearn else "Our", n_clusters, J, init, n_init))
    plt.figure()
    plt.hold(True)
    plt.title("Training data from 'EMGaussian.train' ({} points)\n{} {}-means, distortion {:g}, init {}, n_init {}".format(X.shape[0], "sklearn's" if use_sklearn else "Our", n_clusters, J, init, n_init))
    # Plot the training data colored by clusters
    plt.scatter(X[:, 0], X[:, 1], c=labels, edgecolors='')
    # Plot the clusters centers
    plt.scatter(centroids[:, 0], centroids[:, 1], s=400, marker='o', edgecolors='k', linewidths=2, c='none')
    plt.scatter(centroids[:, 0], centroids[:, 1], s=400, marker='+', edgecolors='k', linewidths=2)  #, c=range(n_clusters))
    # Also plot the initial centroids if available
    # if not use_sklearn:
        # plt.scatter(mu0[:, 0], mu0[:, 1], s=400, marker='*', edgecolors='k', c='none') # , c=range(n_clusters)
    plt.hold(False)
    # Save the plots?
    if doSaveThePlots:
        output = ospathjoin("fig", "{}_Kmeans_init_{}__from_training_data.png".format("sklearn" if use_sklearn else "Our", init))
        print("Saving to", output, "...")
        plt.savefig(output, dpi=160)
        if saveAlsoPDF:
            output = ospathjoin("fig", "{}_Kmeans_init_{}__from_training_data.pdf".format("sklearn" if use_sklearn else "Our", init))
            print("Saving to", output, "...")
            plt.savefig(output, dpi=190)
    plt.show()
    # Compare results
    if print_distortion_measures:
        # means, stds, covs = means_stds_covs(X, labels, n_clusters)
        print("This time, we found these parameters and results:")
        print("  - Centers:", centroids)
        # print("  - Means:", means)
        print("  - Distorsion (inertia):", J)
        # print("  - Distortions 1D (std):", stds.tolist())
        # print("  - Distortions 2D (covariance):", covs)
# End of demo_kmeans

# End of kmeans.py
