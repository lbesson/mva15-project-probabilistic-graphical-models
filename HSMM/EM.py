#! /usr/bin/env python3
# -*- coding: utf-8; mode: python -*-
""" Implementation of the EM algorithm, 2 different versions (question (b) and (c)).

We added a stopping criterion for EM, by computing either the distortion or the log-likelihood:

 - and if it increases (delta > -1e-10) FAIL with an exception,
 - if it did not decrease enough (e.g. delta < 1e-10) stop).
 - But distortion seems to increase from time to time, that's weird FIXME.


Homework assignement was on http://www.di.ens.fr/~slacoste/teaching/MVA_GM/fall2015/Exercises_11nov2015.pdf
With data from http://www.di.ens.fr/~slacoste/teaching/MVA_GM/fall2015/classification_data_HWK2.zip

- *Date:* Wednesday 11 November 2015.
- *Author:* Lilian Besson and Valentin Brunck, for the MVA Master, (C) 2015-16.
- *Licence:* MIT Licence (http://lbesson.mit-license.org).
"""

from __future__ import print_function, division  # Python 2 compatibility

import numpy as np
import numpy.linalg as la
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm

from os.path import join as ospathjoin
from kmeans import k_means  # Local import
use_sklearn = False


# %% (b) EM with constant-diagonal covariance matrices

def N_isotropic(xi, muj, sigmaj, d=2):
    """ N(x_i | mu_j, sigma_j) for the special case of isotropic covariances, drawn from a Gaussian of covar matrix Sj = sigmaj**2 I_n."""
    return np.exp(- (np.dot((xi-muj).T, xi-muj)) / (2 * sigmaj**2)) / (sigmaj**d * (2*np.pi)**(d/2))


def tau_isotropic(X, mu, sigma, weights, n, d, K):
    """ Compute the matrix tau_i^j as used in the lecture notes (very inefficient)."""
    # n, d = np.shape(X)
    # K = np.size(mu,0)
    tau = np.zeros((n, K))
    for i in range(n):
        for j in range(K):
            tau[i, j] = weights[j] * N_isotropic(X[i], mu[j], sigma[j], d=d)
        tau[i] /= np.sum(tau[i])
    return tau


def predict_isotropic(X, mu, sigma, weights, n, d, K):
    """ Predict the most probably label for each X based on the learned parameters of the isotropic GMM (mu, Sigma, weights)."""
    tau = tau_isotropic(X, mu, sigma, weights, n, d, K)
    labels = np.argmax(tau, axis=1)
    return labels


def EM_isotropic(X, n_components, max_iter=40, verb=True):
    """ EM algorithm for covariance matrices proportional to the identity."""
    n, d = np.shape(X)
    K = n_components
    # Initialization with K-means
    mu, z = mu0, z0 = k_means(X, K)[0:2]
    # Initial stds cluster-wise
    # sigma = [np.var(X[z == k]) for k in range(K)]
    sigma = [np.std(X[z == k]) for k in range(K)]
    weights = np.ones(K)
    for k in range(K):
        weights[k] = np.count_nonzero(z == k)
    weights /= n  # Estimated frequencies of clusters
    if verb:
        print("Initialization with K-means done.")
        print(" - Estimated initial centers are:", mu)
        print(" - Estimated initial sigma are:", sigma)
        print(" - Estimated initial weights are:", weights)
    # EM Loop (2 steps)
    for t in range(max_iter):
        # 1. E-step : compute the posterior probabilities tau_i^j
        tau = tau_isotropic(X, mu, sigma, weights, n, d, K)
        # 2. M-step : theta_{t+1} by maximizing L(q_{t+1}, theta) wrt theta
        # The lecture notes gives pretty easy formula for mu, sigma, weights
        w = np.sum(tau, 0)
        mu = (np.dot(X.T, tau) / w).T  # Works OK
        # We should do this in a linear algebra fashion
        sigma = [0 for i in range(K)]
        for j in range(K):
            for i in range(n):
                tmp = np.reshape(X[i] - mu[j], (d, 1))
                sigma[j] += tau[i, j] * np.dot(tmp.T, tmp)
            sigma[j] = np.sqrt(float(sigma[j]) / (d*w[j]))
        weights = w / n
        # Print current parameters
        if verb:
            print("\nStep t =", t)
            print(" - Current centers are:", mu)
            print(" - Current sigma are:", sigma)
            print(" - Current weights are:", weights)
    # We are done
    return mu, sigma, weights


# %% Plot the density estimation

def plot_density_estimation_mine(Xtrain, labels, mu, Sigma, weights, n_clusters, doSaveThePlots=False, general=False, use_sklearn=False, test=False):
    """ Display predicted scores (log prob) by the model as a contour plot."""
    centroids = mu
    n, d = np.shape(Xtrain)
    K = n_clusters
    xtrain, ytrain = Xtrain[:, 0], Xtrain[:, 1]
    # A grid for the contour plot for the log prob
    xmin, xmax = 1.1*np.min(xtrain), 1.1*np.max(xtrain)
    ymin, ymax = 1.1*np.min(ytrain), 1.1*np.max(ytrain)
    x = np.linspace(xmin, xmax)
    y = np.linspace(ymin, ymax)
    X, Y = np.meshgrid(x, y)
    XX = np.array([X.ravel(), Y.ravel()]).T
    # Negative score (Log probabilities of each data point in X)
    tau = tau_general(XX, mu, Sigma, weights, np.size(XX, 0), d, K)
    labelsXX = np.argmax(tau, axis=1)
    Z = np.max(tau, axis=1)
    for i in range(np.size(XX, 0)):
        j = labelsXX[i]
        Z[i] = N_general(XX[i], mu[j], Sigma[j], d=d)
    Z = Z.reshape(X.shape)
    # Start the plot
    plt.figure()
    if test:
        origin = "Testing data from 'EMGaussian.test'"
    else:
        origin = "Training data from 'EMGaussian.train'"
    plt.title("{} ({} points)\n{} GMM with {} covariances, for {} clusters ({}).".format(origin, Xtrain.shape[0], "sklearn's" if use_sklearn else "Our", "full" if general else "isotropic", n_clusters, "ellipsoids" if general else "circles"))
    plt.hold(True)
    # Contour and the colorbar
    CS = plt.contour(X, Y, Z, norm=LogNorm(vmin=1e-6, vmax=1e0),
                     levels=np.logspace(-3, 0, 6))
    CB = plt.colorbar(CS, shrink=0.8, extend='both')
    # Plot the training data colored by clusters
    plt.scatter(xtrain, ytrain, c=labels, edgecolors='')
    # Plot the clusters centers
    plt.scatter(centroids[:, 0], centroids[:, 1], s=400, marker='o', edgecolors='k', linewidths=2, c='none')
    plt.scatter(centroids[:, 0], centroids[:, 1], s=400, marker='+', edgecolors='k', linewidths=2)  # , c=range(n_clusters))
    plt.hold(False)
    if doSaveThePlots:
        output = ospathjoin("fig", "{}_GMM__{}__from_{}ing_data.png".format("sklearn" if use_sklearn else "Our", "full" if general else "isotropic", "test" if test else "train"))
        print("Saving to", output, "...")
        plt.savefig(output, dpi=160)  # dpi=190 for PDF
    plt.axis('tight')
    plt.show()


def plot_density_estimation_gmm(clf, Xtrain, n_clusters, doSaveThePlots=False, general=False, use_sklearn=True, test=False):
    """ Display predicted scores (log prob) by the model as a contour plot."""
    centroids = clf.means_
    xtrain, ytrain = Xtrain[:, 0], Xtrain[:, 1]
    labels = clf.predict(Xtrain)
    # FIXME convert these log-likelihood labels to cluster labels
    # A grid for the contour plot for the log prob
    xmin, xmax = 1.1*np.min(xtrain), 1.1*np.max(xtrain)
    ymin, ymax = 1.1*np.min(ytrain), 1.1*np.max(ytrain)
    x = np.linspace(xmin, xmax)
    y = np.linspace(ymin, ymax)
    X, Y = np.meshgrid(x, y)
    XX = np.array([X.ravel(), Y.ravel()]).T
    Z = -clf.score_samples(XX)[0]  # Negative score (Log probabilities of each data point in X.)
    Z = Z.reshape(X.shape)
    # Start the plot
    plt.figure()
    if test:
        origin = "Testing data from 'EMGaussian.test'"
    else:
        origin = "Training data from 'EMGaussian.train'"
    plt.title("{} ({} points)\n{} GMM with {} covariances, for {} clusters ({}).".format(origin, Xtrain.shape[0], "sklearn's" if use_sklearn else "Our", "full" if general else "isotropic", n_clusters, "ellipsoids" if general else "circles"))
    plt.hold(True)
    # Contour and the colorbar
    CS = plt.contour(X, Y, Z, norm=LogNorm(vmin=1e-2, vmax=1e2),
                     levels=np.logspace(-2, 2, 10))
    CB = plt.colorbar(CS, shrink=0.8, extend='both')
    # Plot the training data colored by clusters
    plt.scatter(xtrain, ytrain, c=labels, edgecolors='')
    # Plot the clusters centers
    plt.scatter(centroids[:, 0], centroids[:, 1], s=400, marker='o', edgecolors='k', linewidths=2, c='none')
    plt.scatter(centroids[:, 0], centroids[:, 1], s=400, marker='+', edgecolors='k', linewidths=2)  # , c=range(n_clusters))
    plt.hold(False)
    if doSaveThePlots:
        output = ospathjoin("fig", "{}_GMM__{}__from_{}ing_data.png".format("sklearn" if use_sklearn else "Our", "full" if general else "isotropic", "test" if test else "train"))
        print("Saving to", output, "...")
        plt.savefig(output, dpi=160)  # dpi=190 for PDF
    plt.axis('tight')
    plt.show()


def demo_EM_isotropic(Xtrain, Xtest, n_components=4, doSaveThePlots=False):
    """ Demo function for the EM algorithm for Mixture of Gaussian, with general covariance matrices."""
    from sklearn import mixture
    gmm_model = mixture.GMM(n_components=n_components,
                            covariance_type='spherical')
                            # covariance_type='diag')
    gmm_model.fit(Xtrain)
    print("gmm_model :", gmm_model)
    means = np.round(gmm_model.means_, 3)
    print("means =", means)
    covars = np.round(gmm_model.covars_, 3)
    print("covars =", covars)
    weights = np.round(gmm_model.weights_, 3)
    print("weights =", weights)

    print("On training data, mean of the log-likelihood for each data point is", np.mean(gmm_model.score(Xtrain)))
    # logprob_train = np.mean(gmm_model.score(Xtrain))
    Ytrain = gmm_model.predict(Xtrain)
    print("Predicted labels:", Ytrain)

    print("Plotting the training data, centers, as well as covariance matrices:")
    plot_density_estimation_gmm(gmm_model, Xtrain, n_components, general=False, doSaveThePlots=doSaveThePlots)

    print("On testing data, mean of the log-likelihood for each data point is", np.mean(gmm_model.score(Xtest)))
    # logprob_test = np.mean(gmm_model.score(Xtest))
    # Ytest = gmm_model.predict(Xtest)


# %% Question (c) EM algorithm with general covariance matrices

def loglikelihood_MoG(clusters, mu, Sigma, weights, K, n):
    """ Compute the log-likelihood of a clusterization for the Mixture of Gaussian model (given its parameters mu, Sigma, weights and K clusters)."""
    loglik = 0
    for k in range(0, K):
        for i in range(0, len(clusters[k])):
            loglik += np.log(weights[k]) - np.log(2*np.pi)
            loglik += -0.5 * np.log(np.linalg.det(Sigma[k]))
            loglik += -0.5 * np.dot(clusters[k][i]-mu[k], np.dot(la.inv(Sigma[k]), clusters[k][i]-mu[k]))
    # return loglik
    # XXX as done in the correction of HMK 2, we divide the log-likelihood by the number of training/testing points (to be able to compare it better, if Ntrain and Ntest are different)
    return loglik / n


def N_general(xi, muj, Sigmaj, d=2):
    """ N(x_i | mu_j, Sigma_j) for the general case (covariance matrices), drawn from a Gaussian of covar matrix Sigmaj."""
    return np.exp(- (np.dot((xi-muj).T, np.dot(la.inv(Sigmaj), xi-muj))) / 2) / np.sqrt(la.det(Sigmaj) * (2*np.pi)**d)


def tau_general(X, mu, Sigma, weights, n, d, K):
    """ Compute the matrix tau_i^j as used in the lecture notes (very inefficient)."""
    # n, d = np.shape(X)
    # K = np.size(mu,0)
    tau = np.zeros((n, K))
    for i in range(n):
        for j in range(K):
            tau[i, j] = weights[j] * N_general(X[i], mu[j], Sigma[j], d=d)
        tau[i] /= np.sum(tau[i])
    return tau


def predict_general(X, mu, Sigma, weights, n, d, K):
    """ Predict the most probably label for each X based on the learned parameters of the general GMM (mu, Sigma, weights)."""
    tau = tau_general(X, mu, Sigma, weights, n, d, K)
    labels = np.argmax(tau, axis=1)
    return labels


def build_clusters_general(X, labels, n, K):
    """ Predict the most probably label for each X based on the learned parameters of the general GMM (mu, Sigma, weights)."""
    clusters = [[] for _ in range(K)]
    for i in range(n):
        x = X[i]
        label_x = labels[i]
        clusters[label_x].append(x.copy())
    return clusters


def distortion_general(X, mu, Sigma, weights, n, d, K):
    r""" The distortion (the cost function we try to minimize), for the dataset X, is defined as:

    J(mu, z) = distortion(X, mu, z) = -sum_i^n sum_k^K z_i^k ||x_i - \mu_k||^2

    Warning: it takes some time (has to re-compute the tau_i and then the labels, and then the distortion).
    """
    labels = predict_general(X, mu, Sigma, weights, n, d, K)
    return np.sum(la.norm(X[labels == k] - mu[k], 2)**2 for k in range(K))


def compute_llh(X, mu, Sigma, weights, n, d, K):
    """ Compute the normalized log-likelihood (the function we try to maximize in EM) for the dataset X with the current MoG parameters.
    """
    labels = predict_general(X, mu, Sigma, weights, n, d, K)
    clusters = build_clusters_general(X, labels, n, K)
    # return n * loglikelihood_MoG(clusters, mu, Sigma, weights, K, n)
    return loglikelihood_MoG(clusters, mu, Sigma, weights, K, n)


def EM_general(X, n_components, max_iter=150, tol=1e-5, verb=True):
    """ EM algorithm for general covariance matrices."""
    n, d = np.shape(X)
    K = n_components
    # Initialization with K-means
    mu, z = mu0, z0 = k_means(X, K)[0:2]
    # Initial variances cluster-wise
    Sigma = np.zeros((K, d, d))  # now we do not have Sj = sj * I_d
    for j in range(K):
        Sigma[j] = np.cov(X[z == j].T)
    weights = np.ones(K)
    for k in range(K):
        weights[k] = np.count_nonzero(z == k)
    weights /= n  # Estimated frequencies of clusters
    current_distortion = distortion_general(X, mu, Sigma, weights, n, d, K)  # Bad initial distortion
    current_llh = compute_llh(X, mu, Sigma, weights, n, d, K)
    if verb:
        print("Initialization with K-means done.")
        print(" - Estimated initial centers are:", mu)
        print(" - Estimated initial Sigma are:", Sigma)
        print(" - Estimated initial weights are:", weights)
        print("EM for MoG : starting the for loop (max_iter = {})".format(max_iter))
        # print("EM for MoG is trying to minimize the distortion...")
        print("EM for MoG is trying to maximize the log-likelihood...")
        print("Initial distortion is {:g}... It will decrease.".format(current_distortion))
        print("Initial log-likelihood is {:g}... It will increase!".format(current_llh))
    # EM Loop (2 steps)
    for t in range(max_iter):
        # 1. E-step : compute the posterior probabilities tau_i^j
        tau = tau_general(X, mu, Sigma, weights, n, d, K)
        # 2. M-step : theta_{t+1} by maximizing L(q_{t+1}, theta) wrt theta
        # The lecture notes gives pretty easy formula for mu, sigma, weights
        w = np.sum(tau, 0)
        mu = (np.dot(X.T, tau) / w).T
        # FIXME We should do this in a linear algebra fashion
        for j in range(K):
            Sigma[j] = np.zeros((d, d))
            for i in range(n):
                tmp = np.reshape(X[i] - mu[j], (d, 1))
                Sigma[j] += tau[i, j] * np.dot(tmp, tmp.T)
            Sigma[j] /= w[j]
        weights = w / n
        # Print current parameters
        if verb:
            print("\nStep t =", t)
            print(" - Current centers are:", mu)
            print(" - Current Sigma are:", Sigma)
            print(" - Current weights are:", weights)
        # Evaluate the new distortion, stop if it has not changed enough
        # FIXME check that this works as wanted
        next_distortion = distortion_general(X, mu, Sigma, weights, n, d, K)
        print("Next distortion is {:g}".format(next_distortion))
        next_llh = compute_llh(X, mu, Sigma, weights, n, d, K)
        # We are trying to MINIMIZE the distortion, so delta_distortion < 0 is required !
        delta_llh = next_llh - current_llh
        print("delta_llh =", delta_llh)
        if delta_llh < - tol:  # 1) BAD !
            print("1) Log-likelihood L = {:g} has decreased too much (delta = {:g} < - tol) ! (with tol = {:g}), that's BAD (at iteration {}).".format(next_llh, delta_llh, tol, t))
            # raise ValueError("EM_general(X, ..): the llh has started to decrease too much ! Fix that bug !")
            # TODO add this ValueError when finish
        elif - tol <= delta_llh < 0:  # 2) Bad but do not stop yet
            print("2) Log-likelihood L = {:g} is decreasing a little bit (- tol <= delta = {:g} < 0, with tol = {:g}), that's BAD but might be OK (at iteration {}).".format(next_llh, delta_llh, tol, t))
        elif 0 <= delta_llh < tol:  # 3) We have converged
            print("3) Log-likelihood L = {:g} has increased just a little bit (0 <= delta = {:g} < tol, with tol = {:g}), so WE STOP NOW at iteration {}).".format(next_llh, delta_llh, tol, t))
            break
        elif tol <= delta_llh:  # 4) Perfect, keep increasing!
            print("4) Log-likelihood L = {:g} has increased enough, continuing (tol <= delta = {:g}, with tol = {:g}) (at iteration {}).".format(next_llh, delta_llh, tol, t))
        else:  # 5) Huh...
            print("5) This case is a bug, it should never happen!")
            raise ValueError("EM_general(X, ..): this else case should never happen. FIXIT !")
        if delta_llh < tol:
            print("Stopping, the log-likelihood has decreased.")
            break
        current_llh = next_llh
    # We are done
    return mu, Sigma, weights


def demo_EM_general(Xtrain, Xtest, n_components=4, doSaveThePlots=False):
    """ Demo function for the EM algorithm for Mixture of Gaussian, with general covariance matrices.

    - Note: require sklearn (see http://scikit-learn.org/ if needed).
    """
    from sklearn import mixture
    gmm_model = mixture.GMM(n_components=n_components, covariance_type='full')
    gmm_model.fit(Xtrain)
    print("gmm_model :", gmm_model)
    means = np.round(gmm_model.means_, 3)
    print("means =", means)
    covars = np.round(gmm_model.covars_, 3)
    print("covars =", covars)
    weights = np.round(gmm_model.weights_, 3)
    print("weights =", weights)

    print("Plotting the training data, centers, as well as covariance matrices:")
    plot_density_estimation_gmm(gmm_model, Xtrain, n_components, general=True, doSaveThePlots=doSaveThePlots)

    print("On training data, mean of the log probability for each data point is", np.mean(gmm_model.score(Xtrain)))
    # logprob_train = np.mean(gmm_model.score(Xtrain))

    print("On testing data, mean of the log probability for each data point is", np.mean(gmm_model.score(Xtest)))
    # logprob_test = np.mean(gmm_model.score(Xtest))


# End of EM.py
