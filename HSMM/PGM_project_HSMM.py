#! /usr/bin/env python3
# -*- coding: utf-8; mode: python -*-
""" Demos of Graphical Models, for the Homework 3 of the Probabilistic Graphical Models course.

Implementations of the requested algorithms were done in sub modules in order to keep the code relatively small.

Homework assignement was on http://www.di.ens.fr/~slacoste/teaching/MVA_GM/fall2015/HWK3_2015.pdf
With data from http://www.di.ens.fr/~slacoste/teaching/MVA_GM/fall2015/classification_data_HWK3.zip

- *Date:* Monday 30th November 2015.
- *Author:* Lilian Besson and Valentin Brunck, for the MVA Master, (C) 2015-16.
- *Licence:* MIT Licence (http://lbesson.mit-license.org).
"""

from __future__ import print_function, division  # Python 2 compatibility

from os.path import join as ospathjoin
import numpy as np
import matplotlib.pyplot as plt
plt.close('all')


# %% Disclaimer, options and data

if __name__ == '__main__':
    print("""
WARNING : this code is NOT AT ALL concluded. It is an academic prototype code.

This is free software, and you are welcome to redistribute it under certain conditions.
This program comes with ABSOLUTELY NO WARRANTY; for details see http://lbesson.mit-license.org/ for more details.
""")

# Option for customizing some part of the code
doInitialPlots = True  # True to generate the plot of the raw initial data
doSaveThePlots = True  # True to save the figures
saveAlsoPDF    = False  # Save also to PDF (default is just PNG)


# Get the training and testing data, once and for all
def centering_2D_data(X):
    """ Center and reduce the 2D data in vector X."""
    sigma_x = X[:, 0].std(axis=0)
    mean_x = np.mean(X[:, 0], axis=0)
    sigma_y = X[:, 1].std(axis=0)
    mean_y = np.mean(X[:, 1], axis=0)
    X[:, 0] = (X[:, 0] - mean_x) / sigma_x
    X[:, 1] = (X[:, 1] - mean_y) / sigma_y
    return X


if __name__ == '__main__':
    print("\n\nReading the datasets and initializing variables.")
    X = np.loadtxt(ospathjoin("data", "EMGaussian.data"))
    print("Centering and renormalizing the data Xtrain...")
    X = centering_2D_data(X)
    Xtrain = X
    Ntrain = np.shape(Xtrain)[0]

    X = np.loadtxt(ospathjoin("data", "EMGaussian.test"))
    print("Centering and renormalizing the data Xtest...")
    X = centering_2D_data(X)
    Xtest = X
    Ntest = np.shape(Xtest)[0]

    n, d = np.shape(X)
    T = n  # Number of points n is also seen as the horizon in the HMM setting
    n_clusters = n_components = K = 4
    print("T = n = {}, d = {}, K = {}.".format(T, d, K))


# %% (HW2) Demo of EM for Mixture of Gaussians (from HW2)
from EM import EM_general  # Local import

if __name__ == '__main__':
    print("\n\n# 0) Implementation of the EM algorithm for Mixture of Gaussians (HMK 2).")
    print("For MoG EM (on train data):")
    mus, Sigmas, weights = EM_general(Xtrain, n_components)
    print("(This was from the last homework...)")


# %% (a) Demo of HSMM on toy example:
import HSMM


if __name__ == '__main__':
    # Filtering
    print("\n## Filtering:")
    filtering_100 = HSMM.filtering_plot(Xtest, K=4, t_max=100)





# %% End
print("\n# For more details, please read the the written report.")
print("(C) 2015-16, Lilian Besson and Valentin Brunck.")
