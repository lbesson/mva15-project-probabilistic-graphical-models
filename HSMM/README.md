## About our implementation
> - For more details, see our [final report](../report/).
> - For the numerical experiments, [see this folder](../src/), e.g. [animated plots we produced for our experiments](./gif.md).

This folder contains our own implementation in [Python (2 or 3)](https://www.python.org/) of :

- [kmeans.py](./kmeans.py) implements the classical K-Means algorithm (with various initializations, including KMeans++),
- [EM.py](./EM.py) implements the Gaussian Mixture Model, mainly the E-M algorithm to train GMM on 2D data,
- [HSMM.py](./HSMM.py) implements the alpha-beta recursion and filtering for [Hidden semi-Markov Models](../), as well as a prototype for a generic E-M algorithm.
- [PGM_project_HSMM.py](./PGM_project_HSMM.py) was used to load [the data](./data/), and produce [these figures](./fig/).

## Filtering on [HMK3 data](./data/EMGaussian.test)
We have 500 points in 2D, generated from 4 Gaussian clusters.
In the data file, the ordering is important: the data are considered as sequential data (time-dependent).

See below the unlabeled plot of the data:

![Initial 2D data from EMGaussian.test file](./fig/Initial_data_from__EMGaussian_test.png "Initial 2D data from EMGaussian.test file")

- Cluster north-east and cluster north-west both generate sequences of length only 1, and the points keep switching from the two clusters, and most of the 500 points are in the two clusters.
- The two other clusters have very less points (and small covariances), but generate longer sequences (usually of length 10, up to 20).

### HMM filtering on 2D data
![HMM filtering on 2D data](./fig/question2__tmax_100.png "HMM filtering on 2D data")

### HSMM filtering on 2D data
![HSMM filtering on 2D data](./fig/filtering_dataHWK2__tmax_100.png "HSMM filtering on 2D data")

*Note:* all this framework is for unsupervised learning: the cluster number (or label) is **not** included in the [datafile](./data/EMGaussian.test)!

### Comparison with non-sequential models
#### Clustering with [K-Means (with K-Means++)](./kmeans.py)
![Clustering with K-Means with K-Means++](./fig/KMeans__on_test_data.png Clustering with K-Means with K-Means++)

#### Clustering with [Gaussian Mixture Model (GMM)](./EM.py)
We implemented an EM algorithm to train a GMM on [train data](./data/EMGaussian.data) and use it to cluster the test datafile:

![Clustering with Gaussian Mixture Model GMM](./fig/Gaussian_Mixture_Model__full_covar__on_test_data.png Clustering with Gaussian Mixture Model GMM)

----

## [Dependencies](../requirements.txt)
- Usual modules: [numpy](http://numpy.org/), [matplotlib](http://matplotlib.org/), [scikit_learn](http://scikit-learn.org/).

----

## About
This project was done for the [Probabilistic Graphical Models](http://www.di.ens.fr/~slacoste/teaching/MVA_GM/fall2015/) course for the [MVA master program](http://www.cmla.ens-cachan.fr/version-anglaise/academics/mva-master-degree-227777.kjsp) at [ENS de Cachan](http://www.ens-cachan.fr).

### Copyright
(C), 2015-16, [Lilian Besson](http://perso.crans.org/besson/) et [Valentin Brunck](http://thesilkmonkey.com/science/science.php) ([ENS de Cachan](http://www.ens-cachan.fr)).

### Licence
This project is publicly published, under the terms of the [MIT license](http://lbesson.mit-license.org/).
