# [*Probabilistic Graphical Models*](http://www.di.ens.fr/~slacoste/teaching/MVA_GM/fall2015/) project (Master MVA)
## Poster
1-page poster for the oral presentation on 09-01-2016.

[See the poster](https://bitbucket.org/lbesson/mva15-project-probabilistic-graphical-models/downloads/MVA_PGM__project_HSSM__Besson_Brunck__Poster_06-01-2016.pdf).

---

## About
This project was done for the [Probabilistic Graphical Models](http://www.di.ens.fr/~slacoste/teaching/MVA_GM/fall2015/) course for the [MVA master program](http://www.cmla.ens-cachan.fr/version-anglaise/academics/mva-master-degree-227777.kjsp) at [ENS de Cachan](http://www.ens-cachan.fr).

### Copyright
(C), 2015-16, [Lilian Besson](http://perso.crans.org/besson/) et [Valentin Brunck](http://thesilkmonkey.com/science/science.php) ([ENS de Cachan](http://www.ens-cachan.fr)).

### Licence
This project is publicly published, under the terms of the [MIT license](http://lbesson.mit-license.org/).
