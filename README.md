# [**Probabilistic Graphical Models**](http://www.di.ens.fr/~slacoste/teaching/MVA_GM/fall2015/) project (Master MVA)
## Meta-data:
- *Subject*: **HMM and HSMM (Hidden Semi-Markovian models)**,
- *Supervisor*: [Guillaume Obozinski](http://imagine.enpc.fr/~obozinsg/teaching.html),
- *Keywords*: HMM, Markov Chain, HSMM,
- *Reference*: [page for the course](http://www.di.ens.fr/~slacoste/teaching/MVA_GM/fall2015/index_projects.html),
- *Where*: [git repository on bitbucket.org](https://bitbucket.org/lbesson/mva15-project-probabilistic-graphical-models),
- *Date*: November 2015 - January 2016,
- *Application*: signal (audio or video) recognition, real-time audio processing.

## *Short Description:*
HSMM (*Hidden Semi-Markovian models*) are a class of models allowing to model the time spent in any given state for a Markov Chain and an HMM.

- Wikipedia pages: [Hidden Markov Models](https://en.wikipedia.org/wiki/Hidden_Markov_model), [Hidden semi-Markov Models](https://en.wikipedia.org/wiki/Hidden_semi-Markov_model).
- [More links are given on this introduction page by K.Murphy](http://www.cs.ubc.ca/~murphyk/Software/HMM/hmm.html).

----

# Main documents
## [Poster](./poster_MLSS2016/) presented at [MLSS](http://mlss.cc/) [2016](http://learning.mpi-sws.org/mlss2016/) (DONE, not presented yet)
- 1-page poster that I will present at the [MLSS 2016](http://learning.mpi-sws.org/mlss2016/) ([Machine Learning Summer School](http://mlss.cc/)) in [Cadiz (Spain)](http://learning.mpi-sws.org/mlss2016/location/) in May 2016.
- [Apparently, I will present it twice](http://learning.mpi-sws.org/mlss2016/poster-sessions/), once at a joint AISTAT/MLSS poster session the first day (Wednesday 11th of May), and a second time the next week (regular slot).
- [See the poster (PDF)](http://lbo.k.vu/posterMLSS2016) ([also here](https://bitbucket.org/lbesson/mva15-project-probabilistic-graphical-models/downloads/MLSS_2016__Poster_on_HMM_and_HSSM__Lilian_Besson__05-2016.en.pdf).

## [Poster for the course](./poster/) (DONE)
- 1-page poster for the oral presentation on 09-01-2016.
- [See the poster](https://bitbucket.org/lbesson/mva15-project-probabilistic-graphical-models/downloads/MVA_PGM__project_HSSM__Besson_Brunck__Poster_06-01-2016.pdf).

## [Final report for the course](./report/) (DONE)
- 5-10-page projet report, due to 13-01-2016.
- [See the report](https://bitbucket.org/lbesson/mva15-project-probabilistic-graphical-models/downloads/MVA_PGM__project_HSSM__Besson_Brunck__Final_report.pdf).

----

## Implementation
- First, have a look to [our implementation of HMM and HSMM](./HSMM/).
- Then, to [the experiments we did](./src/) by using [pyhsmm](https://github.com/mattjj/pyhsmm) and [hmmlearn](https://github.com/hmmlearn/hmmlearn). All the animated graphs are shown [in this document](./src/gif.md).

### References for our implementation
- [GitHub/pyhsmm](https://github.com/mattjj/pyhsmm) is quite awesome.
- [GitHub/hmmlearn](https://github.com/hmmlearn/hmmlearn) is mature, pure Python, close to scikit-learn, with documentation [hmmlearn/doc](https://hmmlearn.readthedocs.org/en/latest), see [hmmlearn/tutorial](https://hmmlearn.readthedocs.org/en/latest/tutorial.html).

----

![Cloud of words for this project](./cloudwords.png "Cloud of words for this project")

> Cloud of words for the Markdown, LaTeX and Python sources for this project. Made with [``generate-word-cloud.py``](https://github.com/Naereen/generate-word-cloud.py/).

## About
This project was done for the [Probabilistic Graphical Models](http://www.di.ens.fr/~slacoste/teaching/MVA_GM/fall2015/) course for the [MVA master program](http://www.cmla.ens-cachan.fr/version-anglaise/academics/mva-master-degree-227777.kjsp) at [ENS de Cachan](http://www.ens-cachan.fr).

> We got a grade of **16/20** for our project, in March 2016.

### Copyright
(C), 2015-16, [Lilian Besson](http://perso.crans.org/besson/) and [Valentin Brunck](http://thesilkmonkey.com/science/science.php) ([ENS de Cachan](http://www.ens-cachan.fr)).

### Licence
This project is publicly published, under the terms of the [MIT license](http://lbesson.mit-license.org/).
