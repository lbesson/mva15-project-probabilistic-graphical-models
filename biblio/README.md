## References
- [This BibTeX file listed all the references we used for our project](./mva15-project-probabilistic-graphical-models.bib).
- [See it as Markdown?](./mva15-project-probabilistic-graphical-models.md).
- Or [HTML](./mva15-project-probabilistic-graphical-models.html).

----

### More references
#### On HMM
- [These slides in French](https://perso.limsi.fr/Individu/allauzen/cours/cours12_13/ASO/cours_HMM.pdf#page=53),
- Wikipédia pages in French : [HSMM](https://fr.wikipedia.org/wiki/Mod%C3%A8le_de_Markov_cach%C3%A9), [Algo de Viterbi](https://fr.wikipedia.org/wiki/Algorithme_de_Viterbi), [Algo de Baum-Welch](https://fr.wikipedia.org/wiki/Algorithme_de_Baum-Welch),
- Wikipédia pages in English: [Fordarw algorithm](https://en.wikipedia.org/wiki/Forward_algorithm).

#### Other references
- [Hidden Markov Models tutorial](http://digital.cs.usu.edu/~cyan/CS7960/hmm-tutorial.pdf) by Phil Blunsom.
- [Hidden Markov Models Fundamentals](http://cs229.stanford.edu/section/cs229-hmm.pdf) by Daniel Ramag.
- [Hidden Markov Models for Speech Recognition](https://www.jstor.org/stable/1268779).
- [A Tutorial on Hidden Markov Models](http://www.robots.ox.ac.uk/~vgg/rg/slides/hmm.pdf) by Marcin Marszalek.
- [A tutorial on hidden Markov models and selected applications in speech recognition](http://www.cs.ubc.ca/~murphyk/Bayes/rabiner.pdf) by Lawrence, R. Rabiner, (1989).

----

## About
This project was done for the [Probabilistic Graphical Models](http://www.di.ens.fr/~slacoste/teaching/MVA_GM/fall2015/) course for the [MVA master program](http://www.cmla.ens-cachan.fr/version-anglaise/academics/mva-master-degree-227777.kjsp) at [ENS de Cachan](http://www.ens-cachan.fr).

### Copyright
(C), 2015-16, [Lilian Besson](http://perso.crans.org/besson/) et [Valentin Brunck](http://thesilkmonkey.com/science/science.php) ([ENS de Cachan](http://www.ens-cachan.fr)).

### Licence
This project is publicly published, under the terms of the [MIT license](http://lbesson.mit-license.org/).
