---|---  
[Mur02]  |  Kevin P. Murphy (2002). _Hidden Semi-Markov Models (HSMMs)_.
<http://www.cs.ubc.ca/~murphyk/mypapers.html>, Unpublished notes.  
[ [bib](mva15-project-probabilistic-graphical-models_bib.html#murphy02) |
[.html](http://www.cs.ubc.ca/~murphyk/mypapers.html) |
[.pdf](http://www.cs.ubc.ca/~murphyk/Papers/segment.pdf) ]  
[Mur14]  |  Kevin P. Murphy (2014). _Hidden Semi-Markov Models (HSMMs)_.
Tutorial slides.  
[ [bib](mva15-project-probabilistic-graphical-models_bib.html#murphy14) |
[.pdf](http://colinlea.com/docs/JournalTalks/2014_HSMM.pdf) ]  
[BL08]  |  Vlad Barbu and Nikolaos Limnios (2008). _Hidden Semi-Markov Model
and Estimation_. In _Semi-Markov Chains and Hidden Semi-Markov Models toward
Applications_, pages 1--48. Springer.  
[ [bib](mva15-project-probabilistic-graphical-models_bib.html#barbu2008) ]  
[Yu10]  |  Shun-Zheng Yu (2010). _Hidden semi-Markov Models_. _Artificial
Intelligence_, 174(2):215--243.  
[ [bib](mva15-project-probabilistic-graphical-models_bib.html#yu10) |
[DOI](http://dx.doi.org/10.1016/j.artint.2009.11.011) |
[.pdf](http://sist.sysu.edu.cn/~syu/Publications/HSMMs_AIJ.pdf) ]  
[OH+11]  |  Jared O'Connell, Søren Højsgaard, _et al._ (2011). _Hidden semi
markov models for multiple observation sequences: The mhsmm package for R_.
_Journal of Statistical Software_, 39(4):1--22.
<https://cran.r-project.org/web/packages/mhsmm/>.  
[ [bib](mva15-project-probabilistic-graphical-models_bib.html#mhsmm) |
[http](https://cran.r-project.org/web/packages/mhsmm/) |
[.pdf](https://cran.r-project.org/web/packages/mhsmm/mhsmm.pdf) ]  
[Rab89]  |  Lawrence R Rabiner (1989). _A tutorial on Hidden Markov Models and
selected applications in speech recognition_. _Proceedings of the IEEE_,
77(2):257--286.  
[ [bib](mva15-project-probabilistic-graphical-models_bib.html#rabiner89) |
[DOI](http://dx.doi.org/10.1109/5.18626) |
[.pdf](http://www.robots.ox.ac.uk:5000/~vgg/rg/papers/hmm.pdf) ]  
[Gue03]  |  Yann Guedon (2003). _Estimating Hidden Semi-markov chains from
discrete sequences_. _Journal of Computational and Graphical Statistics_,
12(3):604--639.  
[ [bib](mva15-project-probabilistic-graphical-models_bib.html#guedon03) |
[DOI](http://dx.doi.org/10.1198/1061860032030) | [.pdf](http://hal.cirad.fr/fi
le/index/docid/826992/filename/JCGSguedon2003.pdf) ]  
[CC14]  |  Philippe Cuvillier and Arshia Cont (2014). _Coherent time modeling
of Semi-Markov Models with application to real-time audio-to-score alignment_.
In _Machine Learning for Signal Processing (MLSP), 2014 IEEE International
Workshop_, pages 1--6. IEEE.  
[ [bib](mva15-project-probabilistic-graphical-models_bib.html#cuvillier14) |
[http](https://hal.inria.fr/hal-01058366/document) ]  
[BBC15]  |  Alberto Bietti, Francis Bach, and Arshia Cont (2015). _An online
EM algorithm in Hidden (Semi-)Markov Models for audio segmentation and
clustering_. In _Acoustics, Speech and Signal Processing (ICASSP), 2015 IEEE
International Conference_, pages 1881--1885.  
[ [bib](mva15-project-probabilistic-graphical-models_bib.html#bach15) |
[DOI](http://dx.doi.org/10.1109/ICASSP.2015.7178297) |
[http](https://hal.inria.fr/hal-01115826/document) ]  
[NCC+15]  |  Eita Nakamura, Philippe Cuvillier, Arshia Cont, Nobutaka Ono, and
Shigeki Sagayama (2015). _Autoregressive Hidden Semi-markov Model of symbolic
music performance for score following_. In _16th International Society for
Music Information Retrieval Conference (ISMIR)_. Malaga, Spain.  
[ [bib](mva15-project-probabilistic-graphical-models_bib.html#nakamura15) | [.
pdf](https://hal.inria.fr/hal-01183820/file/draft_SemiMarkovMIDIScofo_ISMIR201
5_v17.pdf) ]
[Lc15]  |  Sergei Lebedev and GitHub contributors (October 2015).
_Unsupervised learning and inference of Hidden Markov Model in Python
[hmmlearn] (GitHub repository)_. <https://github.com/hmmlearn/hmmlearn/>,
online, accessed 28.12.2015.  
[ [bib](mva15-project-probabilistic-graphical-models_bib.html#hmmlearn) |
[http](https://github.com/hmmlearn/hmmlearn/) ]  
[Jc15]  |  Matthew J. Johnson and GitHub contributors (December 2015).
_Bayesian inference in HSMMs and HMMs in Python [pyhsmm] (GitHub repository)_.
<https://github.com/mattjj/pyhsmm>, online, accessed 28.12.2015.  
[ [bib](mva15-project-probabilistic-graphical-models_bib.html#pyhsmm) |
[http](https://github.com/mattjj/pyhsmm) ]  
[JW13]  |  Matthew J. Johnson and Alan S. Willsky (February 2013). _Bayesian
non-parametric Hidden semi-Markov Models (HDP-HSMM)_. _Journal of Machine
Learning Research_, 14:673--701.  
[ [bib](mva15-project-probabilistic-graphical-models_bib.html#johnson13) |
[.pdf](http://www.jmlr.org/papers/volume14/johnson13a/johnson13a.pdf) ]  
[Cap01]  |  Olivier Cappe (2001). _Ten years of HMMs_. <http://perso.telecom-
paristech.fr/~cappe/Research/Bibliographies/hmmbib/>, Unpublished notes on-
line.  
[ [bib](mva15-project-probabilistic-graphical-models_bib.html#cappe01) |
[http](http://perso.telecom-
paristech.fr/~cappe/Research/Bibliographies/hmmbib/) ]  
[CMR06]  |  Olivier Cappe, Eric Moulines, and Tobias Ryden (2006). _Inference
in Hidden Markov Models_. Springer Science &amp; Business Media.  
[ [bib](mva15-project-probabilistic-graphical-models_bib.html#cappe06) ]  
[KF09]  |  Daphne Koller and Nir Friedman (2009). _Probabilistic Graphical
Models_. Adaptive Computation And Machine Learning. MIT Press.  
[ [bib](mva15-project-probabilistic-graphical-models_bib.html#koller09) ]  
[BP66]  |  Leonard E Baum and Ted Petrie (1966). _Statistical inference for
probabilistic functions of finite state Markov chains_. _The Annals of
Mathematical Statistics_, pages 1554--1563.  
[ [bib](mva15-project-probabilistic-graphical-models_bib.html#baumpetrie) |
[DOI](http://dx.doi.org/10.1214/aoms/1177699147) |
[http](http://projecteuclid.org/download/pdf_1/euclid.aoms/1177699147) ]  
[Bis06]  |  Christopher M Bishop (2006). _Pattern Recognition And Machine
Learning_. Springer.  
[ [bib](mva15-project-probabilistic-graphical-models_bib.html#bishop06) ]  
[Jor98]  |  Michael I. Jordan (1998). _Learning in Graphical Models_, volume
89\. Springer Science &amp; Business Media. <http://cognet.mit.edu/book
/learning-graphical-models>, proceedings of the NATO Advanced Study Institute
(Ettore Mairona Center, Erice, Italy).  
[ [bib](mva15-project-probabilistic-graphical-
models_bib.html#jordan98learningpgm) | [http](http://cognet.mit.edu/book
/learning-graphical-models) ]  
[Jor04]  |  Michael I. Jordan (2004). _Graphical Models_. _Statistical
Science_, 19(1):140--155. ISSN 08834237.  
[ [bib](mva15-project-probabilistic-graphical-
models_bib.html#jordan98graphical) ]  
  
* * *

_This file was generated by
[bibtex2html](http://www.lri.fr/~filliatr/bibtex2html/) 1.98._

