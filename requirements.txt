numpy >= 1.9.3
matplotlib >= 1.4.3
scikit_learn >= 0.17
daft >= 0.0.3
hmmlearn >= 0.1.1
pyhsmm >= 0.1.5
